var FOOBALL = FOOBALL || {};

FOOBALL.newTeam = function(name, color, human, direction) {
	//no access ...
	var privateVars = {
		activePlayer : 10
	};
	var team = {
		name : name,
		color : color,
		human : human,
		direction : direction,
		players : [],
		setActivePlayer : function(selected) {
			console.log("selecting "+selected);
			privateVars.activePlayer = selected;
			for(var i=0; i<this.players.length; i++) {
				this.players[i].active = i === privateVars.activePlayer;
			}
		},
		getActivePlayer : function() {
			return privateVars.activePlayer;
		},
		closestExcept : function(ai) {
			var closestDist= 1;
			var closestIndex=null;
			for(var i=0; i<this.players.length; i++) {
				if(i!=ai) {
					var a = this.players[i];
					var dist= FOOBALL.physics2d.distance(a.posVector.getXy(), FOOBALL.game.ball.posVector.getXy());
					if(dist < closestDist) {
						closestIndex= i;
						closestDist= dist;
					}
				}
			}
			return closestIndex;
		},
		switchPlayer : function() {
			var ai= this.getActivePlayer();
			this.switchPlayerExcept(ai);
		},
		switchPlayerExcept : function(ai) {
			var closestIndex= this.closestExcept(ai);
			if(closestIndex!=null) {
				this.setActivePlayer(closestIndex);
			} else {
				console.log("Warning - closest player not found!!");
			}
		},
		init : function() {
			this.players = [];
			var formation = FOOBALL.formations.FourFourTwo;
			var playernum=0;
			for(var i=0; i<formation.length; i++) {
				var row= formation[i];
				//console.log(row);
				for(var j=0; j<row.yoffs.length; j++) {
					this.players[playernum]= Object.create(FOOBALL.entities.Player);
					this.players[playernum].number = playernum;
					this.players[playernum].base = { xindex : j, xcount : row.yoffs.length, yoffs : row.yoffs[j] };
					this.players[playernum].teamDirection = this.direction;
					this.players[playernum].human = this.human;
					var xys = FOOBALL.draw.calcStartpoint(this.direction, row.yoffs[j], j, row.yoffs.length);
					this.players[playernum].startPoint= xys;
					var xyb = FOOBALL.draw.calcBasepoint(this.direction, row.yoffs[j], j, row.yoffs.length);
					this.players[playernum].basePoint= xyb;
					this.players[playernum].mainColor = this.color;
					this.players[playernum].positionName = row.pos;
					this.players[playernum].posVector= FOOBALL.physics2d.newVector(0,0);
					this.players[playernum].posVector.setXy(xys);
					this.players[playernum].speedVector= FOOBALL.physics2d.newVector(0,0);
					playernum++;
				}
			}
			this.setActivePlayer(this.getActivePlayer());
		},

		update : function(modifier) {
			for(var i=0; i<this.players.length;i++) {
				var currentPlayer = this.players[i];
				if(currentPlayer.moving()) {
					currentPlayer.clear(FOOBALL.mainview);
					currentPlayer.clear(FOOBALL.radarview);
					var moved = currentPlayer.move(modifier);
				}
			}
		}
	};
	team.init();
	return team;
};

FOOBALL.newBall = function () {
	ball = Object.create(FOOBALL.entities.Ball);
	ball.posVector = FOOBALL.physics2d.newVector(45, 0.705);
	ball.speedVector = FOOBALL.physics2d.newVector(0, 0);
	return ball;
};

FOOBALL.newGame = function() {
	var team1 = FOOBALL.newTeam('Red Rackams', '#990000', true, true),
	    team2 = FOOBALL.newTeam('Blue Meanies', '#000099', false, false);
	return {
		paused : false,
		ball : FOOBALL.newBall(),
		timer : { },
		score : [0,0],
		keysDown : {},
		pause : function(isPause) {
			this.paused = isPause;
		},
		team1 : team1,
		team2 : team2,
		update : function(modifier) {
			if(!this.paused) {
				// Update game objects
				this.team1.update(modifier);
				this.team2.update(modifier);
				var players = [];
				players= players.concat(this.team1.players,this.team2.players);
				this.ball.collide(null, players);
				if (this.ball.move(modifier)) {
					this.ball.clear(FOOBALL.mainview);
					this.ball.clear(FOOBALL.radarview);
				}
			}
		},
		keyUp : function(e) {
			delete FOOBALL.game.keysDown[e.keyCode];
		},
		keyDown : function(e) {
			FOOBALL.game.keysDown[e.keyCode] = true;
		},
		keyPress : function(e) {
			switch(String.fromCharCode(e.keyCode)) {
				case 'a':
				case 'A':
					team1.switchPlayer();
					break;
				default:
					console.log("key code "+e.keyCode);
					//nothing
			}
		}
	};
};

