
var FOOBALL = FOOBALL || {};
FOOBALL.ws = {
	keys : { 38 : 'UP', 40 : 'DOWN', 37 : 'LEFT', 39 : 'RIGHT' },
	socket : null,
	init : function() {
		document.addEventListener("keydown", function (e) {
				FOOBALL.ws.processKeyDown(e.keyCode);
		}, false);

		document.addEventListener("keyup", function (e) {
				FOOBALL.ws.processKeyUp(e.keyCode);
		}, false);
		//Moz browser support ...
		  if (!window.WebSocket && window.MozWebSocket) {
		    window.WebSocket = window.MozWebSocket;
		  }
		this.setupHandler();

	},
	processKeyUp : function(keyCode) {
		if (this.keys[keyCode]) {
			this.send(this.keys[keyCode]+" off");
		}
	},
	processKeyDown : function(keyCode) {
		if (this.keys[keyCode]) {
			this.send(this.keys[keyCode]+" on");
		}
	},
	send : function(message) {
	    if (!window.WebSocket) { return; }
	    if (this.socket.readyState == WebSocket.OPEN) {
	      this.socket.send(message);
	    } else {
	      alert("The socket is not open.");
	    }
	},
	setupHandler : function() {
	  if (window.WebSocket) {
	    this.socket = new WebSocket("ws://localhost:8888/websocket/");
	    this.socket.onmessage = function(event) { var ta = document.getElementById('responseText'); ta.value = ta.value + '\n' + event.data };
	    this.socket.onopen = function(event) { var ta = document.getElementById('responseText'); ta.value = "Web Socket opened!"; };
	    this.socket.onclose = function(event) { var ta = document.getElementById('responseText'); ta.value = ta.value + "Web Socket closed"; };
	  } else { 
		//TODO: fallback option
	    alert("Your browser does not support Web Sockets.");
	  }
	}

};

