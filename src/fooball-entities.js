/*jslint browser: true */
var FOOBALL = FOOBALL || {};

FOOBALL.extend = function(destination, source) {
	for (var prop in source) {
		if (source.hasOwnProperty(prop)) {
			destination[prop] = source[prop];
		}
	}
};

FOOBALL.entities= {
	Entity : {
		clear : function(view) {
			if(this.lastPos == null) {
				this.lastPos= this.posVector.getXy();
			}
			var xy= view.transformPoint(this.lastPos);
			view.context.clearRect(xy[0]-1,xy[1]-1,2,2);
		},
		xlimit : function(mod) {
			if(!this.posVector.inside([0-mod,0-mod],[1+mod,1+mod])) {
				var xy= this.posVector.getXy();
				if(xy[0]>1+mod) { xy[0]=1+mod; }
				if(xy[1]>1+mod) { xy[1]=1+mod; }
				if(xy[0]<0-mod) { xy[0]=0-mod; }
				if(xy[1]<0-mod) { xy[1]=0-mod; }
				this.posVector.setXy(xy);
				return true;
			}
			return false;
		},
		limitAxis : function(mod,which) {
			var xy= this.posVector.getXy();
			if (xy[which] < 0-mod) {
				xy[which]= 0-mod;
				this.posVector.setXy(xy);
				return true;
			}
			if (xy[which] > 1+mod) {
				xy[which]= 1+mod;
				this.posVector.setXy(xy);
				return true;
			}
			return false;
		},
		limitX : function(mod) {
			return this.limitAxis(mod, 0);
		},
		limitY : function(mod) {
			return this.limitAxis(mod, 1);
		},
		limit : function(mod) {
			this.limitX(mod);
			this.limitY(mod);
		}
	}
};

FOOBALL.entities.Ball = {
	lastPos : null,
	//posVector : FOOBALL.physics2d.newVector(45, 0.705),
	//speedVector : FOOBALL.physics2d.newVector(0, 0),
	mainColor : 'orange',
	draw : function(view) {
		var xy = this.posVector.getXy();
		xy= view.transformPoint(xy);
		FOOBALL.draw.drawDisc(
			view.context,
			xy,
			0.5,
			this.mainColor
		);
	},
	move : function(modifier) {
		if(this.speedVector.magnitude > 0) {
			this.lastPos = this.posVector.getXy();
			//update
			this.posVector.add( FOOBALL.physics2d.mul(this.speedVector, modifier) );
			//account for friction and collisions
			//TODO only if pointing away from midpoint
			var limited=false;
			if(this.limitX(0)) {
				this.speedVector.bounceX();
				limited=true;
			}
			if(this.limitY(0)) {
				this.speedVector.bounceY();
				limited=true;
			}
			//if (!limited) {
			this.speedVector.mul(FOOBALL.behaviourTypes.ball.friction);
			//}
			return true;
		} else {
			return false;
		}
	},
	collide : function(immovables, players) {
		var closestDist= 1;
		var closest=null;
		for(var i=0; i<players.length; i++) {
			var a = players[i];
			var dist= FOOBALL.physics2d.distance(a.posVector.getXy(), this.posVector.getXy());
			if(dist < 0.025 && dist < closestDist) {
				closest= a;
				closestDist= dist;
			}
		}
		if (closest != null) {
			this.speedVector.become(closest.kickVector());

		}
	}
};
FOOBALL.extend(FOOBALL.entities.Ball, FOOBALL.entities.Entity);
FOOBALL.entities.Player = {
	number : -1,
	base : { xindex : 0, xcount : 0, yoffs : 0 },
	teamDirection : true,
	posVector : null,
	lastPos : null,
	speedVector : null,
	positionName : '',
	mainColor : 'blue',
	active : false,
	human : false,
	isHoofing : function() {
		if (this.human && this.active) {
			if (FOOBALL.keyCodes.SPACE in FOOBALL.game.keysDown) { // Player holding space
				return true;
			}
		}
		return false;
	},
	isKicking : function() {
		if (this.human && this.active) {
			if (FOOBALL.keyCodes.Z in FOOBALL.game.keysDown) { // Player holding space
				return true;
			}
		}
		return false;
	},
	isPassing : function() {
		if (this.human && this.active) {
			if (FOOBALL.keyCodes.X in FOOBALL.game.keysDown) { // Player holding space
				return true;
			}
		}
		return false;
	},
	kickVector : function() {
		//TODO replace hardcoded team1 refs
		if(this.isPassing()) {
			var targetNumber = FOOBALL.game.team1.closestExcept(this.number);
			var angle = FOOBALL.physics2d.angle(this.posVector.getXy(), FOOBALL.game.team1.players[targetNumber].posVector.getXy());
			if(this.human) {
				//switch to closest player after pass
				setTimeout(function() { FOOBALL.game.team1.switchPlayerExcept(-1); }, 100);
			}
			return FOOBALL.physics2d.newVector(angle, this.speedVector.magnitude * 8);
		} else if(this.isHoofing() {
			if(this.human) {
				//switch to closest player after pass
				setTimeout(function() { FOOBALL.game.team1.switchPlayerExcept(-1); }, 100);
			}
			return FOOBALL.physics2d.newVector(this.speedVector.angle, this.speedVector.magnitude * 10);
		} else if(this.isKicking()) {
			if(this.human) {
				//switch to closest player after pass
				setTimeout(function() { FOOBALL.game.team1.switchPlayerExcept(-1); }, 100);
			}
			return FOOBALL.physics2d.newVector(this.speedVector.angle, this.speedVector.magnitude * 5);
		} else {
			return FOOBALL.physics2d.newVector(this.speedVector.angle, this.speedVector.magnitude * 2);
		}
	},
	draw : function(view) {
		var xy= view.transformPoint(this.posVector.getXy());
		FOOBALL.draw.drawDisc(
			view.context,
			xy,
			0.6,
			this.mainColor
			);
		if(this.active) {
			FOOBALL.draw.drawDisc(
				view.context,
				xy,
				0.3,
				'#EEEEEE'
				);
		}
	},
	moving : function(modifier) {
		if (this.human && this.active) {
			if (Object.keys(FOOBALL.game.keysDown).length>0) {
				if (FOOBALL.keyCodes.UP in FOOBALL.game.keysDown
					|| FOOBALL.keyCodes.DOWN in FOOBALL.game.keysDown
					|| FOOBALL.keyCodes.LEFT in FOOBALL.game.keysDown
					|| FOOBALL.keyCodes.RIGHT in FOOBALL.game.keysDown

						) { // Player holding up
					return true;
				}
			}
			return false;
		} else if (this.active) {
			return true;
		} else {
			var disToBase = FOOBALL.physics2d.distance(this.posVector.getXy(), this.basePoint);
			if(disToBase > 0.05) {
				return true;
			}
			return false;
		}
	},
	move : function(modifier) {
		var posXy= this.posVector.getXy();
		this.lastPos= posXy;
		if (this.human && this.active) {
			if (Object.keys(FOOBALL.game.keysDown).length>0) {
				//moving. Undraw before moving
				var angleCalced=false;
				if (FOOBALL.keyCodes.UP in FOOBALL.game.keysDown) { // Player holding up
					posXy[1] -= this.speedVector.magnitude * modifier;
					this.speedVector.angle = 270;
					if(FOOBALL.keyCodes.LEFT in FOOBALL.game.keysDown) {
						this.speedVector.angle -= 45;
					} else if(FOOBALL.keyCodes.RIGHT in FOOBALL.game.keysDown) {
						this.speedVector.angle += 45;
					}
					angleCalced=true;
				}
				if (FOOBALL.keyCodes.DOWN in FOOBALL.game.keysDown) { // Player holding down
					posXy[1] += this.speedVector.magnitude * modifier;
					this.speedVector.angle = 90;
					if(FOOBALL.keyCodes.LEFT in FOOBALL.game.keysDown) {
						this.speedVector.angle += 45;
					} else if(FOOBALL.keyCodes.RIGHT in FOOBALL.game.keysDown) {
						this.speedVector.angle -= 45;
					}
					angleCalced=true;
				}
				if (FOOBALL.keyCodes.LEFT in FOOBALL.game.keysDown) { // Player holding left
					posXy[0] -= this.speedVector.magnitude * modifier;
					if(!angleCalced) {
						this.speedVector.angle = 180;
					}
					angleCalced=true;
				}
				if (FOOBALL.keyCodes.RIGHT in FOOBALL.game.keysDown) { // Player holding right
					posXy[0] += this.speedVector.magnitude * modifier;
					if(!angleCalced) {
						this.speedVector.angle = 0;
					}
					angleCalced=true;
				}
				//this.posVector.setXy(posXy);
				//TODO gradual
				this.speedVector.magnitude=0.2;
			} else {

				//TODO gradual
				this.speedVector.magnitude=0;
			}
		} else if (this.active) {
			var angToBall = FOOBALL.physics2d.angle(this.posVector.getXy(), FOOBALL.game.ball.posVector.getXy());
			var angToGoal = FOOBALL.physics2d.angle(this.posVector.getXy(), this.teamDirection ? [0.5,0] : [0.5,1]);
			var disToBall = FOOBALL.physics2d.distance(this.posVector.getXy(), FOOBALL.game.ball.posVector.getXy());
			//adjust angle depending on target goal
			var angToGo = FOOBALL.physics2d.veerAway(angToBall, angToGoal);
			this.speedVector.magnitude= 0.1;
			if(disToBall > 0.02) {
				this.speedVector.angle = angToGo;
			} else {
			//	this.speedVector.magnitude= 0;
				this.speedVector.angle = angToGoal;
			}

		//	console.log(this.posVector.getXy());
			//console.log(this.speedVector.angle);
		} else {
			var angToBase = FOOBALL.physics2d.angle(this.posVector.getXy(), this.basePoint);
			var disToBase = FOOBALL.physics2d.distance(this.posVector.getXy(), this.basePoint);
			if(disToBase > 0.05) {
				this.speedVector.magnitude= 0.1;
				this.speedVector.angle = angToBase;
			} else {
				this.speedVector.magnitude= 0;
			}
		}
		this.posVector.add( FOOBALL.physics2d.mul(this.speedVector, modifier) );
		//limit player to pitch (plus 0.025)
		this.limit(0.025);
		return this.speedVector.magnitude > 0;
	}
};
FOOBALL.extend(FOOBALL.entities.Player, FOOBALL.entities.Entity);

